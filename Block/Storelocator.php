<?php
namespace FME\GoogleMapsStoreLocator\Block;
 
use FME\GoogleMapsStoreLocator\Model\ResourceModel\Storelocator\CollectionFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\ObjectManagerInterface;
use Magento\Directory\Model\RegionFactory;

class Storelocator extends Template
{
          
    protected $scopeConfig;
    protected $collectionFactory;
    protected $objectManager;
    public $googleMapsStoreHelper;
    protected $regionFactory;
        
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \FME\GoogleMapsStoreLocator\Helper\Data $helper,
        CollectionFactory $collectionFactory,
        ObjectManagerInterface $objectManager,
        RegionFactory $regionFactory
    ) {
        
        $this->collectionFactory = $collectionFactory;
        $this->objectManager = $objectManager;
        $this->googleMapsStoreHelper = $helper;
        $this->regionFactory = $regionFactory;
        parent::__construct($context);
    }

    public function _prepareLayout()
    {
        if ($this->googleMapsStoreHelper->isEnabledInFrontend()) {
            $this->pageConfig->setKeywords($this->googleMapsStoreHelper->getGMapMetaKeywords());
            $this->pageConfig->setDescription($this->googleMapsStoreHelper->getGMapMetadescription());
            $this->pageConfig->getTitle()->set($this->googleMapsStoreHelper->getGMapPageTitle());
  
            return parent::_prepareLayout();
        }
    }

    public function getAllStores()
    {
       
        $collection = $this->collectionFactory->create()->addFieldToFilter('is_active', 1)
        ->setOrder('city', 'ASC');
        return $collection;
    }

    /**
     * @param CollectionFactory $store
     * @return mixed
     */
    public function getFullAddress($store)
    {
        return $store->getAddressStreet() . ', ' . $store->getAddressNumber() .
                ' - ' . $store->getAddressNeighborhood() . ', ' .
                $store->getCity() . ' - ' . $this->getRegion($store->getRegionId()) .
                ' - ' . $store->getZipcode();
    }

    /**
     * @param $regionId int
     */
    public function getRegion($regionId)
    {
        $region = $this->regionFactory->create();
        $region->load($regionId);
        return $region->getData('code');
    }

    public function getRegionData($regionId)
    {
        $region = $this->regionFactory->create();
        $region->load($regionId);
        return $region->getData();
    }
}
