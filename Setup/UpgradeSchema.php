<?php
namespace FME\GoogleMapsStoreLocator\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;

        $installer->startSetup();
        if (version_compare($context->getVersion(), '1.0.0.1', '<')) {
            $installer->getConnection()->dropColumn(
                $setup->getTable('fme_googlemapsstorelocator'),
                'address');
            $installer->getConnection()->addColumn(
                $installer->getTable('fme_googlemapsstorelocator'),
                'address_street',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => false,
                    "comment" => "Street Address"
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('fme_googlemapsstorelocator'),
                'address_number',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 15,
                    'nullable' => false,
                    "comment" => "Number Address"
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('fme_googlemapsstorelocator'),
                'address_neighborhood',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => false,
                    "comment" => "Neighborhood Address"
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('fme_googlemapsstorelocator'),
                'city',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => false,
                    "comment" => "City"
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('fme_googlemapsstorelocator'),
                'region_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => 10,
                    'nullable' => false,
                    "comment" => "Region Id"
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('fme_googlemapsstorelocator'),
                'telephone',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 20,
                    'nullable' => true,
                    "comment" => "Telephone"
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('fme_googlemapsstorelocator'),
                'zipcode',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 20,
                    'nullable' => false,
                    "comment" => "ZipCode"
                ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.0.3', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('fme_googlemapsstorelocator'),
                'store_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => 10,
                    'nullable' => true,
                    'comment' => 'Store ID associated with the record'
                ]
            );
        }
        $installer->endSetup();
    }
}
